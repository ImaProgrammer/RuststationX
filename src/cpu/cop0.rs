#[derive(Debug)]
pub struct Cop0 {
    reg_indx: u32,
    reg_rand: u32,
    reg_tlbl: u32,
    reg_bpc: u32,
    reg_ctxt: u32,
    reg_bda: u32,
    reg_pidmask: u32,
    reg_dcic: u32,
    reg_badv: u32,
    reg_bdam: u32,
    reg_tlbh: u32,
    reg_bpcm: u32,
    reg_sr: u32,
    reg_cause: u32,
    reg_epc: u32,
    reg_prid: u32,
    reg_erreg: u32
}

impl Cop0 {
    pub fn new() -> Cop0 {
        Cop0 {
            reg_indx: 0,
            reg_rand: 0,
            reg_tlbl: 0,
            reg_bpc: 0,
            reg_ctxt: 0,
            reg_bda: 0,
            reg_pidmask: 0,
            reg_dcic: 0,
            reg_badv: 0,
            reg_bdam: 0,
            reg_tlbh: 0,
            reg_bpcm: 0,
            reg_sr: 0,
            reg_cause: 0,
            reg_epc: 0,
            reg_prid: 0,
            reg_erreg: 0
        }
    }

    // Call this function when an exception is activated and return the address
    //pub fn set_exception(&mut self, exception: Exception, pc: u32) -> u32 {
    //
    //}
}

// Exception types
#[derive(Copy, Clone)]
pub enum Exception {
    // Interrupt Request
    Interrupt = 0x0,
    // Valid bit is set but a dirty bit isn't set
    TLBModified = 0x1,
    // TLB entry valid bit isn't set
    TLBMissLoad = 0x2,
    TLBMissStore = 0x3,
    // Address load error
    AddressLoadError = 0x4,
    // Address store error
    AddressStoreError = 0x5,
    // Invalid physical address error
    BusInvalidPhysicalError = 0x6,
    // Invalid access type
    BusInvalidDataType = 0x7,
    // System Call trap instruction
    SystemCall = 0x8,
    // Break instruction
    Breakpoint = 0x9,
    // Execution of an instruction with an undefined or reserved opcode
    ReservedInstruction = 0xA,
    // Execution of a co-processor instruction when the it is not set for the target co-processor
    CoprocessorUnusable = 0xb
}
