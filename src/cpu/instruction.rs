#[derive(Copy, Clone)]
pub struct Instruction(pub u32);

impl Instruction {
    // Return bits 31:26 of the instruction
    pub fn function(self) -> u32 {
        let Instruction(opcode) = self;

        opcode >> 26
    }

    // Return register index in bits 20:16
    pub fn t(self) -> u32 {
        let Instruction(opcode) = self;

        (opcode >> 16) & 0x1f
    }

    // Return immediate value in bits 16:0
    pub fn immediate(self) -> u32 {
        let Instruction(opcode) = self;

        opcode & 0xffff
    }
}
