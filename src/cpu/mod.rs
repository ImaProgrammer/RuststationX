mod instruction;
mod opcode;

use interconnect;

pub struct Cpu {
    //cop0: cop0::Cop0,
    // General purpose registers
    reg_zr: u32,
    reg_at: u32,
    reg_v: [u32; 2],
    reg_a: [u32; 4],
    reg_t: [u32; 8],
    reg_s: [u32; 8],
    reg_mt: [u32; 2],
    reg_gp: u32,
    reg_sp: u32,
    reg_fp: u32,
    reg_ra: u32,
    // General multiply/divide result registers
    reg_hi: u32,
    reg_lo: u32,
    reg_pc: u32,
    // Memory interface
    interconnect: interconnect::Interconnect,
    next_instruction: instruction::Instruction
}

impl Cpu {
    pub fn new(interconnect: interconnect::Interconnect) -> Cpu {
        Cpu {
            // General purpose registers
            reg_zr: 0,
            reg_at: 0,
            reg_v: [0; 2],
            reg_a: [0; 4],
            reg_t: [0; 8],
            reg_s: [0; 8],
            reg_mt: [0; 2],
            reg_gp: 0,
            reg_sp: 0,
            reg_fp: 0,
            reg_ra: 0,
            // General multiply/divide result registers
            reg_hi: 0,
            reg_lo: 0,
            reg_pc: 0xbfc0_0000, // PC reset value at the beginning of the BIOS
            //cop0: cop0::Cop0::new(),
            interconnect: interconnect,
            next_instruction: instruction::Instruction(0x0)
        }
    }

    pub fn power_on_reset(&mut self) {
        self.reg_pc = 0xbfc0_0000;
    }

    pub fn next_instruction(&mut self) {
        let reg_pc = self.reg_pc;

        // Use previously loaded instruction
        let instruction = self.next_instruction;

        // Fetch instruction at reg_pc
        self.next_instruction = instruction::Instruction(self.load_32(reg_pc));

        // Print our next instruction
        println!("reg_pc {:#018X}", self.reg_pc);

        // Increment reg_pc to point to the next instruction
        self.reg_pc = reg_pc.wrapping_add(4);

        self.decode_and_execute(instruction);
    }

    pub fn decode_and_execute(&mut self, instruction: instruction::Instruction) {
        match instruction.function() {
            //opcode::LuI => self.op_lui(instruction),
            _ => panic!("Unhandled Instruction {:08x}", instruction.0),
        }
    }

    pub fn load_32(&self, addr: u32) -> u32 {
        self.interconnect.load_32(addr)
    }

    fn register(&self, index: u32) -> u32 {
        unimplemented!();
    }

    fn set_register(&mut self, index: u32, value: u32) {
        unimplemented!();
    }

    // Load Upper Immediate
    fn op_lui(&mut self, instruction: instruction::Instruction) {
        let imm = instruction.immediate();
        let t = instruction.t();

        panic!("What now?");
    }
}
