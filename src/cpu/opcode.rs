pub const Special: u32 = 0b000000;
pub const LuI: u32 = 0b001111;

// CPU Arithmetic instructions
pub const Add: u32 = 0b100000;
pub const AddI: u32 = 0b001000;
pub const AddIU: u32 = 0b001001;
pub const AddU: u32 = 0b100001;

//Clo: u32 = 0b100001;
//Clz: u32 = 0b100000;

pub const Div: u32 = 0b011010;
pub const DivU: u32 = 0b011011;

//Madd = 0b000000;
pub const MaddU: u32 = 0b000001;
//Msub = 0b000100;
pub const MsubU: u32 = 0b000101;
//Mul = 0b000010;
pub const MulT: u32 = 0b011000;
pub const MulTU: u32 = 0b011001;

pub const Slt: u32 = 0b101010;
pub const SltI: u32 = 0b001010;
pub const SltIU: u32 = 0b001011;
pub const SltU: u32 = 0b101011;
pub const Sub: u32 = 0b100010;
pub const SubU: u32 = 0b100011;

/*
// CPU Branch and Jump instructions
//B = 0b;
//Bal = 0b;
//Beq = 0b000100;
Bgez = 0b010100;
//BgezAL = 0b00001;
//Bgtz = 0b000111;
//Blez = 0b000110;
//Bltz = 0b;
BltzAL = 0b10000;
//Bne = 0b000101;

//J = 0b000010;
//JAL = 0b000011;
//JALR = 0b001001;
//JR = 0b001000;

// CPU Instruction Control instructions
//Nop = 0b;
//Ssnop = 0b;

// CPU Load; Store and Memory control instructions
//Lb = 0b100000;
//LbU = 0b100100;
//Lh = 0b100001;
//LhU = 0b100101;
//Ll = 0b110000;
//Lw = 0b100011;
//LwL = 0b100010;
//LwR = 0b100110;

Pref = 0b110011;

Sb = 0b101000;
Sc = 0b111000;
//Sd = 0b;
//Sh = 0b101001;
//Sw = 0b101011;
//SwL = 0b101010;
//SwR = 0b101110;
//SyncMem = 0b001111;
// CPU Logical instructions
And = 0b001100;
AndI = 0b100100;
//LuI = 0b001111;
Nor = 0b100111;
Or = 0b100101;
OrI = 0b001101;
Xor = 0b100110;
XorI = 0b001110;

// CPU Move instructions
//Mfhi = 0b010000;
//Mflo = 0b010010;
//Movf = 0b;
//Movn = 0b001011;
//Movt = 0b;
//Movz = 0b001010;
//Mthi = 0b010001;
//Mtlo = 0b010011;

// CPU Shift instruction
//Sll = 0b000000;
SllV = 0b000100;
Sra = 0b000011;
SraV = 0b000111;
Srl = 0b000010;
SrlV = 0b000110;

// CPU Trap instructions
Break = 0b;
Syscall = 0b;
Teq = 0b;
TeqI = 0b;
Tge = 0b;
TegI = 0b;
TegIU = 0b;
TegU = 0b;
Tlt = 0b;
TltI = 0b;
TltIU = 0b;
TltU = 0b;
Tne = 0b;
TneI = 0b;
*/
