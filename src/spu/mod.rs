/*
    RuststationX: Emulator for the Sony Playstation System
    Written by and copyright (C) 2016 ImaProgrammer
*/

// Sound Processing Unit
pub struct Spu {
    // SPU Random Access Memory: 512kb, 24 Voices
    ram: [u32; 512 * 1024],
}
