/*
    RuststationX: Emulator for the Sony Playstation System
    Written by and copyright (C) 2016 ImaProgrammer
*/

mod cpu;
mod memory;
mod interconnect;
mod psx;

use std::env;

fn main() {
    let bios_file = env::args().nth(1).unwrap();
    let psx = &mut psx::PSX::new(bios_file);
    //psx.power_on_reset();
    psx.run();
}
