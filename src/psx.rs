/*
    RuststationX: Emulator for the Sony Playstation System
    Written by and copyright (C) 2016 ImaProgrammer
*/

use std::path;
use super::{cpu, interconnect, memory};

pub struct PSX {
    cpu: cpu::Cpu
}

impl PSX {
    pub fn new(bios_file: String) -> PSX {
        // TODO allow different location of BIOS file due to hardcoded address!
        let bios = memory::Bios::new(&path::Path::new(&bios_file)).unwrap();
        let interconnect = interconnect::Interconnect::new(bios);
        let cpu = cpu::Cpu::new(interconnect);

        PSX {
            cpu: cpu
        }
    }

    pub fn power_on_reset(&mut self) {
        self.cpu.power_on_reset();
    }

    pub fn run(&mut self) {
        loop {
            self.cpu.next_instruction();
        }
    }
}
