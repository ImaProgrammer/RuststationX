/*
    RuststationX: Emulator for the Sony Playstation System
    Written by and copyright (C) 2016 ImaProgrammer
*/

/*
    Playstation Memory Map (from the documentation by Joshua Walker)

    0x0000_0000-0x0000_ffff     Kernel(64K)
    0x0001_0000-0x001f_ffff     User Memory (1.9 Meg)
    0x1f00_0000-0x1f00_ffff     Parallel Port (64K)
    0x1f80_0000-0x1f80_03ff     Scratch Pad (1024 bytes)
    0x1f80_1000-0x1f80_2fff     Hardware Registers (8K)
    0x8000_0000-0x801f_ffff     Kernel and User Memory Mirror (2 Meg) Cached
    0xa000_0000-0xa01f_ffff     Kernel and User Memory Mirror (2 Meg) Uncached
    0xbfc0_0000-0xbfc7_ffff     BIOS (512K)
*/

use std::io;
use std::io::Read;
use std::fs;
use std::path;
use std::result;


pub const KERNEL_START: u32 = 0x0000_0000;
pub const KERNEL_END: u32 = 0x0000_ffff;
pub const USER_MEM_START: u32 = 0x0001_0000;
pub const USER_MEM_END: u32 = 0x001f_ffff;
pub const PARALLEL_PORT_START: u32 = 0x1f00_0000;
pub const PARALLEL_PORT_END: u32 = 0x1f00_ffff;
pub const SCRATCH_PAD_START: u32 = 0x1f80_0000;
pub const SCRATCH_PAD_END: u32 = 0x1f80_03ff;
pub const HARDWARE_REG_START: u32 = 0x1f80_1000;
pub const HARDWARE_REG_END: u32 = 0x1f80_2fff;
pub const KERNEL_USER_MEM_MIRROR_CACHED_START: u32 = 0x8000_0000;
pub const KERNEL_USER_MEM_MIRROR_CACHED_END: u32 = 0x801f_ffff;
pub const KERNEL_USER_MEM_MIRROR_UNCACHED_START: u32 = 0xa000_0000;
pub const KERNEL_USER_MEM_MIRROR_UNCACHED_END: u32 = 0xa01f_ffff;
pub const BIOS_START: u32 = 0xbfc0_0000;
pub const BIOS_END: u32 = 0xbfc7_ffff;

pub enum Addr {
    /*
    Kernel(u32),
    UserMem(u32),
    ParallelPort(u32),
    ScratchPad(u32),
    HardwareReg(u32),
    KernelUserMemMirrorCached(u32),
    KernelUserMemMirrorUnCached(u32),
    */
    Bios(u32)
}

pub struct Range(u32, u32);

impl Range {
    // Return Some(offset) if addr is contained in self
    pub fn contains(self, addr: u32) -> Option<u32> {
        let Range(start, length) = self;

        if addr >= start && addr < start + length {
            Some(addr - start)
        } else {
            None
        }
    }
}

pub const BIOS: Range = Range(0xbfc0_0000, 512 * 1024);

// TODO Add PS2 BIOS Size
const BIOS_SIZE: u64 = 512 * 1024;

// The BIOS image
pub struct Bios {
    // BIOS data
    data: Vec<u8>,
    // Is the BIOS a PS2 BIOS or a PS1 BIOS
    ps2_bios: bool
}

impl Bios {
    // Load a BIOS image from the file located at path
    pub fn new(path: &path::Path) -> result::Result<Bios, io::Error> {
        let file = try!(fs::File::open(path));
        let mut data = Vec::new();
        let ps2_bios = false;

        // Load the BIOS
        try!(file.take(BIOS_SIZE).read_to_end(&mut data));

        if data.len() == BIOS_SIZE as usize {
            Result::Ok(Bios {data: data, ps2_bios: ps2_bios})
        } else {
            Result::Err(io::Error::new(io::ErrorKind::InvalidInput, "Invalid BIOS size"))
        }
    }

    // Fetch the 32 bit little endian word at offset
    pub fn load_32(&self, offset: u32) -> u32 {
        let offset = offset as usize;

        let b0 = self.data[offset + 0] as u32;
        let b1 = self.data[offset + 1] as u32;
        let b2 = self.data[offset + 2] as u32;
        let b3 = self.data[offset + 3] as u32;

        b0 | (b1 << 8) | (b2 << 16) | (b3 << 24)
    }
}
