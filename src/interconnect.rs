/*
    RuststationX: Emulator for the Sony Playstation System
    Written by and copyright (C) 2016 ImaProgrammer
*/

use std::path;

use super::memory;

// Global interconnect
pub struct Interconnect {
    bios: memory::Bios
}

impl Interconnect {
    pub fn new(bios: memory::Bios) -> Interconnect {
        Interconnect {
            bios: bios
        }
    }

    pub fn load_32(&self, addr: u32) -> u32 {
        if let Some(offset) = memory::BIOS.contains(addr) {
            return self.bios.load_32(offset);
        }
        
        panic!("Unhandled fetch_32 at address {:08x}", addr);
    }
}
