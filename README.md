![RuststationX](http://i.imgur.com/RPIvz6l.png)

* [About The Project](#about)
* [Contact](#contact)
* [Team Members](#team-members)

<h1 name="about">About The Project</h1>
This project was created out of interest of the Playstation 1 due to it being simple but so influential that it changed the console face forever.
Currently the project is still in it's infant stages and is unable to play games.

<h1 name="contact">Contact</h1>
If you liked to talk to us or anyone in general about RuststationX please head over to #ruststationx on lolwutnet.
<h1> animal-industries: apex.animal-industries.xyz Channel: #ruststationx </h1>

# <a name="team-members"></a>Team Members
* TechnoCrunch/ImaProgrammer
